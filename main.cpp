#include "stdlib.h"
#include "stdio.h"
#include "string.h"

#include "Windows.h"
#include "utility.h"

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

#define STB_IMAGE_WRITE_IMPLEMENTATION
#define __STDC_LIB_EXT1__ // required for visual studio to not cry about safety
#include "stb_image_write.h"

#define STB_JSON_IMPLEMENTATION
#include "stb_json.h"

struct RGBA {
	u8 r;
	u8 g;
	u8 b;
	u8 a;
};

struct Image {
	u32 w;
	u32 h;
	RGBA* data;
};

Image new_image(u32 w, u32 h) {
	Image new_image = {};
	new_image.w = w;
	new_image.h = h;
	new_image.data = (RGBA*)calloc(w * h, sizeof(RGBA));
	return new_image;
}

Image load_image(char* filename) {
	Image image = {};
	image.data = (RGBA*)stbi_load(filename, (int*)&image.w, (int*)&image.h, 0, 4);
	return image;
}

void free_image(Image* image) {
	if (image->data) free(image->data);
	*image = {};
}

void save_image(Image* image, char* filename) {
	stbi_write_png(filename, image->w, image->h, 4, image->data, image->w * sizeof(RGBA));
}

Image image_from_crop(Image* image, u32 x, u32 y, u32 w, u32 h, u32 new_w, u32 new_h, u32 off_x, u32 off_y) {
	Image crop_image = new_image(new_w, new_h);
	for (u32 xx = 0; xx < w; ++xx) {
		for (u32 yy = 0; yy < h; ++yy) {
			if (xx + x >= image->w || yy + y >= image->h) {
				continue;
			}
			crop_image.data[(xx + off_x) + (yy + off_y) * crop_image.w] = image->data[(xx + x) + (yy + y) * image->w];
		}
	}
	return crop_image;
}

void paste_image(Image* image, Image* pasted, u32 x, u32 y) {
	for (u32 xx = 0; xx < pasted->w; ++xx) {
		for (u32 yy = 0; yy < pasted->h; ++yy) {
			if (xx + x >= image->w || yy + y >= image->h) {
				continue;
			}
			image->data[(xx + x) + (yy + y) * image->w] = pasted->data[xx + yy * pasted->w];
		}
	}
}

f64 normalized_color_distance(RGBA a, RGBA b) {
	f64 a_r = a.r;
	f64 a_g = a.g;
	f64 a_b = a.b;
	f64 a_dist = sqrt(pow(a_r, 2) + pow(a_g, 2) + pow(a_b, 2));
	a_r /= a_dist;
	a_g /= a_dist;
	a_b /= a_dist;

	f64 b_r = b.r;
	f64 b_g = b.g;
	f64 b_b = b.b;
	f64 b_dist = sqrt(pow(b_r, 2) + pow(b_g, 2) + pow(b_b, 2));
	b_r /= b_dist;
	b_g /= b_dist;
	b_b /= b_dist;
	
	return sqrt(pow(a_r - b_r, 2) + pow(a_g - b_g, 2) + pow(a_b - b_b, 2)); // We don't care about alpha
}

f64 color_distance(RGBA a, RGBA b) {
	return sqrt(pow(a.r - b.r, 2) + pow(a.g - b.g, 2) + pow(a.b - b.b, 2)); // We don't care about alpha
}

bool is_purple(RGBA color) {
	const RGBA purple = {255, 71, 255, 255};
	return normalized_color_distance(color, purple) < 0.35;
}

RGBA find_closest_purple(RGBA color) {
	const RGBA purples[8] = {
		{25,  0, 25,  255},
		{58,  0, 58,  255},
		{91,  0, 91,  255},
		{124, 0, 124, 255},
		{156, 0, 156, 255},
		{189, 0, 189, 255},
		{222, 0, 222, 255},
		{255, 0, 255, 255},
	};
	u32 best_purple = 0;
	f64 best_distance = UINT_MAX;
	for (u32 i = 0; i < 8; ++i) {
		f64 distance = color_distance(color, purples[i]);
		if (distance < best_distance) {
			best_purple = i;
			best_distance = distance;
		}
	}
	return purples[best_purple];
}

f32 truncate_color(s32 color) {
	if (color < 0) return 0;
	if (color > 255) return 255;
	return color;
}

RGBA increase_color_contrast(RGBA color, f32 factor) {
	color.r = truncate_color((color.r - 128) * factor + 128);
	color.g = truncate_color((color.g - 128) * factor + 128);
	color.b = truncate_color((color.b - 128) * factor + 128);
	return color;
}

void process_image_color(Image* image) {
	for (u32 x = 0; x < image->w; ++x) {
		for (u32 y = 0; y < image->h; ++y) {
			RGBA* color = &image->data[x + y * image->w];
			// Make all not fully transparent pixels opaque
			if (color->a == 0) {
				continue;
			}
			color->a = 255;

			// Make sure we don't use pure black (transparency key color)
			if (color->r + color->g + color->b <= 32) {
				color->r = max(color->r, 8);
				color->g = max(color->g, 8);
				color->b = max(color->b, 8);
			}

			// Make all team color brood war compatible
			if (is_purple(*color)) {
				*color = find_closest_purple(increase_color_contrast(*color, 1.5));
			}
		}
	}
}

void add_border(Image* image, RGBA color) {
	for (u32 x = 0; x < image->w; ++x) {
		u32 x2 = image->w - 1 - x;
		u32 y2 = image->h - 1;
		image->data[x  + 0  * image->w] = color;
		image->data[x2 + y2 * image->w] = color;
	}
	for (u32 y = 0; y < image->h; ++y) {
		u32 x2 = image->w - 1;
		u32 y2 = image->h - 1 - y;
		image->data[0  + y  * image->w] = color;
		image->data[x2 + y2 * image->w] = color;
	}
}

#define FrameCap 1000
struct Frame {
	char filename[256];

	u32 x;
	u32 y;
	u32 w;
	u32 h;

	u32 off_x;
	u32 off_y;
	u32 total_w;
	u32 total_h;
};

int main(int argc, char** argv) {
	char name[256] = {};
	printf("Input graphic name: ");
	scanf_s("%s", name, sizeof(name));
	
	u32 facing_angles = 0;
	while (facing_angles != 1 && facing_angles != 16 && facing_angles != 32) {
		printf("Specify the original amount of facing angles (has to be 1 or 16 or 32): ");
		scanf_s("%u", &facing_angles);
	}

	const char* images_json = "data/images.json";
	char gfx_data_path[256] = {};
	sprintf_s(gfx_data_path, "data/%s.json", name);

	String gfx_json = {};
	bool uses_global_index = false;
	if (file_exists(gfx_data_path)) {
		gfx_json = load_entire_file(gfx_data_path);
		printf("Using %s for atlas data...\n", gfx_data_path);
	} else if (file_exists(images_json)) {
		printf("Using %s for atlas data...\n", images_json);
		gfx_json = load_entire_file(images_json);
		uses_global_index = true;
	} else {
		printf("No valid atlas data file!\n");
		return 0;
	}

	stbj_cursor cursor = stbj_load_buffer(gfx_json.data, gfx_json.size);
	if (stbj_any_error(&cursor)) {
		printf("JSON error!\n");
	} else {
		if (uses_global_index) {
			cursor = stbj_move_cursor_name(&cursor, name);
		}
		cursor = stbj_move_cursor_name(&cursor, "frames");

		char path[256] = {};
		sprintf_s(path, "data/%s.png", name); // TODO: Update the path to be correct
		Image spritesheet = load_image(path);
		if (!spritesheet.data) {
			printf("There is no matching spritesheet!\n");
			goto exit;
		}

		if (!CreateDirectoryA(name, 0) && GetLastError() != ERROR_ALREADY_EXISTS) {
			free_image(&spritesheet);
			printf("Couldn't create folder %s!\n", name);
			goto exit;
		}

		u32 min_x1 = UINT_MAX;
		u32 min_y1 = UINT_MAX;
		u32 max_x2 = 0;
		u32 max_y2 = 0;

		u32 frame_count = 0;
		static Frame frames[FrameCap] = {};

		u32 size = stbj_count_values(&cursor);
		for (u32 i = 0; i < size; ++i) {
			if (frame_count >= FrameCap) {
				break;
			}
			char filename[256] = {};
			stbj_cursor object_cursor = stbj_move_cursor_index(&cursor, i);
			stbj_read_string_name(&object_cursor, "filename", filename, 256, "");
			if (!filename[0] || str_contains(filename, "gui")) {
				continue;
			}
			Frame* frame = &frames[frame_count++];
			strcpy_s(frame->filename, filename);

			stbj_cursor frame_cursor = stbj_move_cursor_name(&object_cursor, "frame");
			frame->x = stbj_read_int_name(&frame_cursor, "x", 0);
			frame->y = stbj_read_int_name(&frame_cursor, "y", 0);
			frame->w = stbj_read_int_name(&frame_cursor, "w", 0);
			frame->h = stbj_read_int_name(&frame_cursor, "h", 0);

			stbj_cursor source_size_cursor = stbj_move_cursor_name(&object_cursor, "sourceSize");
			frame->total_w = stbj_read_int_name(&source_size_cursor, "w", 0);
			frame->total_h = stbj_read_int_name(&source_size_cursor, "h", 0);

			stbj_cursor sprite_source_size_cursor = stbj_move_cursor_name(&object_cursor, "spriteSourceSize");
			frame->off_x = stbj_read_int_name(&sprite_source_size_cursor, "x", 0);
			frame->off_y = stbj_read_int_name(&sprite_source_size_cursor, "y", 0);

			u32 x1 = frame->off_x;
			u32 y1 = frame->off_y;
			u32 x2 = frame->off_x + frame->w;
			u32 y2 = frame->off_y + frame->h;

			min_x1 = min(min_x1, x1);
			min_y1 = min(min_y1, y1);
			max_x2 = max(max_x2, x2);
			max_y2 = max(max_y2, y2);
		}

		u32 minimal_frame_w = max_x2 - min_x1;
		u32 minimal_frame_h = max_y2 - min_y1;
		u32 frame_w = (minimal_frame_w + 31) / 32 * 32;
		u32 frame_h = (minimal_frame_h + 31) / 32 * 32;
		u32 off_x = (frame_w - minimal_frame_w) / 2;
		u32 off_y = (frame_h - minimal_frame_h) / 2;

		u32 frame_multiplier = facing_angles == 16 ? 2 : 1;
		u32 tile_w = facing_angles == 1 ? 17 : 32;
		u32 tile_h = (frame_count * frame_multiplier) / tile_w;
		u32 tile_count = tile_w * tile_h;
		u32 tile_reminder = (frame_count * frame_multiplier) % tile_w;
		u32 tile_x = facing_angles == 1 ? 0 : (tile_w + 1) / 2;
		u32 tile_y = 0;

		Image new_spritesheet = new_image(frame_w * tile_w, frame_h * (tile_reminder ? tile_h + 1 : tile_h));

		for (u32 i = 0; i < frame_count; ++i) {
			Frame* frame = &frames[i];
			printf("Processing %s (%ux%u)...\n", frame->filename, frame_w, frame_h);

			Image frame_image = image_from_crop(&spritesheet, frame->x, frame->y, frame->w, frame->h, frame_w, frame_h, frame->off_x - min_x1 + off_x, frame->off_y - min_y1 + off_y);
			process_image_color(&frame_image);
			// add_border(&frame_image, {255, 0, 0, 255});

			// char filepath[256] = {};
			// sprintf_s(filepath, "%s/%s", name, frame->filename);
			// save_image(&frame_image, filepath);

			if (i * frame_multiplier == tile_count) {
				tile_x = 0;
				tile_y = tile_h;
			}
			paste_image(&new_spritesheet, &frame_image, tile_x * frame_w, tile_y * frame_h);
			if (facing_angles == 16) {
				paste_image(&new_spritesheet, &frame_image, (tile_x + 1) * frame_w, tile_y * frame_h);
			}

			if (i * frame_multiplier >= tile_count) {
				tile_x += frame_multiplier;
			} else if (facing_angles == 1) {
				tile_x += 1;
				if (tile_x >= tile_w) {
					tile_x = 0;
					tile_y += 1;
					tile_y %= tile_h;
				}
			} else {
				tile_y += 1;
				if (tile_y >= tile_h) {
					tile_y = 0;
					tile_x += frame_multiplier;
					tile_x %= tile_w;
				}
			}


			free_image(&frame_image);
		}

		char filepath[256] = {};
		sprintf_s(filepath, "%s/%s.png", name, name);
		printf("Saving spritesheet %s (%ux%u)...\n", name, tile_w * frame_w, tile_h * frame_h);
		save_image(&new_spritesheet, filepath);
		free_image(&new_spritesheet);

		free_image(&spritesheet);
	}
exit:
	free_string(&gfx_json);
	return 0;
}