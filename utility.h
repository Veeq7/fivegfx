#pragma once

typedef char s8;
typedef short s16;
typedef int s32;
typedef long long s64;

typedef unsigned char u8;
typedef unsigned short u16;
typedef unsigned int u32;
typedef unsigned long long u64;

typedef float f32;
typedef double f64;

struct String {
	u64 size;
	char* data;
};

String load_entire_file(const char* filename);
void free_string(String* string);

bool str_contains(const char* a, const char* b);
bool file_exists(const char* filename);