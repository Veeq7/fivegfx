#include "utility.h"

#include "stdio.h"
#include "stdlib.h"
#include "string.h"

String load_entire_file(const char* filename) {
	FILE* file = 0;
	fopen_s(&file, filename, "rb");
	if (!file) {
		printf("File %s doesn't exist!\n", filename);
		return {};
	}

	String string = {};
	fseek(file, 0, SEEK_END);
	string.size = ftell(file);
	fseek(file, 0, SEEK_SET);

	string.data = (char*)malloc(string.size + 1);
	fread(string.data, 1, string.size, file);
	string.data[string.size] = 0;

	fclose(file);
	return string;
}

void free_string(String* string) {
	if (string->data) free(string->data);
	*string = {};
}

bool str_contains(const char* a, const char* b) {
	u32 a_len = strlen(a);
	u32 b_len = strlen(b);
	for (u32 i = 0; i < a_len - b_len; ++i) {
		for (u32 j = 0; j < b_len; ++j) {
			if (a[i + j] != b[j]) {
				goto next;
			}
		}
		return true;
	next:
		continue;
	}
	return false;
}

bool file_exists(const char* filename) {
	FILE* file = 0;
	fopen_s(&file, filename, "rb");
	if (file) {
		fclose(file);
		return true;
	}
	return false;
}